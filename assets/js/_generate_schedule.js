/*jshint esversion: 6 */
//-----------------------------------------
// ----- START OF GENERATE SCHEDULE -------
//-----------------------------------------
//get major from colleges
let scheduleData = [];
const generateSchedule = () =>{
    //hide elements not pertaining to schedule
    $('#natural-sciences, #generate-schedule, #select-menu').hide();
    console.log(localStorage);
    //get information based on college and major
     $.ajax({
        url: baseUrl+'assets/json/colleges_and_majors.json?v='+versionNumber,
        dataType: 'json',
        error: function(xhr){
            error(xhr);
        },
        success: function(data){
            //console.log('assets/json/colleges_and_majors.json?v='+versionNumber);
            //call getCollege() function
            getColleges(data);
        }
    });
}; //end of generateSchedule()


const getColleges = (data) =>{
    
    data.forEach(function(data){
        
        data.majors.forEach(function(majors){
            if(majors.major === localStorage.major){
                console.log(majors);
                if( majors.foreignLanguageRequired === false){
                    localStorage.foreign_language = false;
                }else{
                    localStorage.foreign_language = true;
                }
                
                scheduleData.push(majors);
                appendSchedule(scheduleData);

                //loop through required courses
                if(majors.requiredCourses.languageSection !== ""){
                    checkMajorForRequired(data);
                }
                
             }
        });
        
    });  
};

//append schedule data
const appendSchedule = (scheduleData) => {
    console.log(localStorage);
   // if($('#schedule').length !==1){

    $('#majors').append(`<div id="schedule"><div class="content"><h2 id="semester-schedule">Semester 1 Example Schedule</h2><pre><strong>Major:</strong> ${localStorage.major}<br/><strong>College:</strong> ${localStorage.college}<br/><strong>Aleks Score:</strong> ${localStorage.aleks}<br/></pre>`);

    $('#schedule').append(`<div class="half"><div class="box"><div id="math"><pre><strong>Math:</strong><br/> <span id="math-course">${localStorage.math}</span>: (<span id="math-credits">${localStorage.math_credits}</span> credits)</pre><pre id="english-course"><strong>English Course:</strong><br/> <span id="english-course">${localStorage.englishCourse}</span>: (<span id="english-credits">${localStorage.englishCredits}</span> Credits)</pre></div><div id="foreign-language"></div><div id="firstyearseminar"></div><div id="required"></div><h4>General Education Options</h4><div id="science"></div><div id="humanities"></div><div id="arts"></div><div id="social"></div><pre class="total"><strong>Total Credits:</strong> <span id="total-credits"></span></div><button type="button" id="reset" class="btn-error inline">Reset</button><button type="button" id="save" class="btn-success inline">Save</button></div></div><div class="half"><pre id="science-courses"></pre><pre id="humanities-courses"></pre><pre id="arts-courses"></pre><pre id="social-courses"></pre><pre id="foreign-language-courses"></pre></div></div>`);

   
    console.log(scheduleData[0]);

    //if foreign language within the last 4 years, append correct language course
    if(scheduleData[0].foreignLanguageRequired === true && localStorage.foreign_four_year === 'Yes'){
        let languageSection;
        if(localStorage.foreign_language_section === '001 - First year'){
            languageSection = '001';
        }
        if(localStorage.foreign_language_section === '002 - Second year'){
            languageSection = '001';
        }
        if(localStorage.foreign_language_section === '003 - Third year'){
             languageSection = '002';
        }
        if(localStorage.foreign_language_section === '004 - Fourth year'){
             languageSection = '003';
        }
        $('#schedule #foreign-language').append(`<pre id="foreign-language-course"><strong>Foreign Language Course:</strong><br/><span id="foreign-lang-course">${localStorage.foreign_language_type} ${languageSection}</span>: (<span id="foreign-lang-credits">4</span> Credits)</pre></pre>`);
     }
     //if foreign language NOT within the last 4 years, append 001 course
     if(scheduleData[0].foreignLanguageRequired === true && localStorage.foreign_four_year === 'No'){
          $('#schedule #foreign-language').append(`<pre id="foreign-language-course"><strong>Foreign Language Course:</strong><br/><span id="foreign-lang-course">${localStorage.foreign_language_type} 001</span>: (<span id="foreign-lang-credits">4</span> Credits)</pre></pre>`);         
    }

    if(localStorage.foreign_language === "false"){
        $('#schedule #foreign-language').html('');
    }

    if(localStorage.getItem('firstSeminarClass') !== null){
        $('#firstyearseminar').append(`<pre><strong>First Year Seminar:</strong><br/><span id="seminar-course">${localStorage.firstSeminarClass}</span>: (<span id="seminar-credits">${localStorage.firstSeminarCredits}</span> Credits)</pre>`);
    }
                        
    scheduleData[0].genEd.forEach(function(genEd){
        console.log(genEd.type);
       if(genEd.type === 'science'){
            $('#schedule #science').append('<pre><strong>Science Course:</strong><br/><button id="select-science" class="top-margin btn-green" type="button">Select a science gen ed</button><span id="science-class"></span></pre>');
             //getCourses('assets/json/sciences.json', 'science');
        }
        if(genEd.type === 'humanities'){
            $('#schedule #humanities').append('<pre><strong>Humanities Course:</strong><br/><button id="select-humanities" class="top-margin btn-green" type="button">Select a humanities gen ed</button><span id="humanities-class"></span></pre>');
            //getCourses('assets/json/humanities.json', 'humanities');
        }
       if(genEd.type === 'arts'){
            $('#schedule #arts').append('<pre><strong>Arts Course:</strong><br/><button id="select-arts" class="top-margin btn-green" type="button">Select an art gen ed</button><span id="arts-class"></span></pre>');
            //getCourses('assets/json/arts.json', 'arts');
        }
        if(genEd.type === 'social'){
             $('#schedule #social').append('<pre><strong>Social and Behavioral Sciences Course:</strong><br/><button id="select-social" class="top-margin btn-green" type="button">Select an Social and Behavioral Sciences gen ed</button><span id="social-class"></span></pre>');
             //getCourses('assets/json/social.json', 'social');
         }
    });
       
     //set total credits value
    updateTotal();

    if(localStorage.englishSemester === 'Spring'){
        $('#english-course').remove();
     }
    $('#semester-schedule').focus();                
 };

 const checkMajorForRequired = (data) =>{
    // data.forEach(function(data){
        data.majors.forEach(function(majors){
            if(majors.major === localStorage.major){
                let count = 0;
                majors.requiredCourses.forEach(function(courses){
                     //Science majors
                     count++;
                    if(courses.course === 'CHEM 110'){
                        if(localStorage.math !== 'MATH 140'){
                            localStorage.setItem(`required_credits${count}`, '4');
                            localStorage.setItem(`required_course${count}`, 'CHEM 108');
                            $('#schedule #science').append(`<pre><strong>Major Required Science Course:</strong><br/><span id="required-class${count}">CHEM 108</span> (<span id="required-class-${count}-credits">4</span> Credits)</span></pre>`);
                        }else{
                            localStorage.setItem(`required_credits${count}`, courses.credits);
                            localStorage.setItem(`required_course${count}`, courses.course);
                            $('#schedule #science').append(`<pre><strong>Major Required Science Course:</strong><br/><span id="required-class${count}">${courses.course}<//span> (<span id="required-class-${count}-credits">${courses.credits}</span> Credits)</span></pre>`);
                            console.log('required science fired');

                        }
                    }else{
                        localStorage.setItem(`required_credits${count}`, courses.credits);
                        localStorage.setItem(`required_course${count}`, courses.course);
                        $('#schedule #required').append(`<pre><strong>Major Required Course:</strong><br/><span id="required-class${count}">${courses.course}</span> (<span id="required-class-${count}-credits">${courses.credits}</span> Credits)</span></pre>`);
                        console.log('required credits fired');
                    }
                    console.log('local storage: ');
                    console.log(localStorage);
                    updateTotal();

                });
               
            }
            
        });
   // });
 };