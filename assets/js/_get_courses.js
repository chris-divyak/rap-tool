/*jshint esversion: 6 */
//-------------------------------------------
// ----- START OF SCIENCE DATA FETCH --------
//-------------------------------------------
const getCourses = (fetchUrl, type) => {
    $(`#${type}-courses`).hide();
    
        $.ajax({
            url: baseUrl+fetchUrl,
            dataType: 'json',
            success: function(data){
                getCourseDescriptions(data,type);
                //check if checkboxes are checked
                checkCheckbox(type);
                console.log(data);
            },
            error: function(xhr){
                errors(xhr);
            }
        });
     
   
    removeBox(type);
};//end of getNaturalSciences
//check if checkboxes are checked or not
const checkCheckbox = (type) =>{
        $(`#${type}-courses .input input[type="checkbox"]`).on('change', function(){
            var checked = $(`#${type}-courses input:checked`).length;
            //check if checkboxes are checked.
            $('#'+type+'-courses input').each(function(){
                if(checked > 0){
                    if($(this).is(':checked')){
                        //set localStorage for selected course
                        localStorage.setItem(type+'_credits', $(this).data('credits'));
                        localStorage.setItem(type+'_course', $(this).data('course'));
                        localStorage.setItem(type+'_title', $(this).data('classname'));
                        console.log(localStorage);
                        //update total
                        updateTotal();
                        //append course to science div
                        $(`#${type}-class`).html(`<span id="${type}-course">${$(this).data('course')}</span>: (<span id="${type}-credits">${$(this).data('credits')}</span> credits) <i class="fa fa-remove" id="delete-${type}"><span>delete</span></i>`);
                    }
                    else{
                        //disable all unchecked checkboxes if one is checked
                        $(this).attr('disabled', true);
                    }
                }
                else{
                    //if none checked, enable all checkboxes
                    $(this).attr('disabled', false);
                }

            });

            $('#'+type+'-courses').hide();

        }).bind(this);
};//end of checkCheckbox()

//remove science class from selected class
const removeBox = (type) =>{
    $(document).on('click', `#delete-${type}`, function(){
        $(this).remove();
        //remove course text
        $(`#${type}-class`).html('');
        

        //reset localStorage
        localStorage.removeItem(`${type}_course`);
        localStorage.removeItem(`${type}_credits`);
        localStorage.removeItem(`${type}_title`);
        console.log(localStorage);
        console.log('removed');
        //remove credits
        updateTotal();
        //enable all checkboxes again and uncheck checked box
        $(`#${type}-courses .input input[type="checkbox"]`).each(function(){
            $(this).attr('disabled', false);
            $(this).attr('checked', false);
        });
        //show list of courses
        $(`#select-${type}`).show();
    });
};


const getCourseDescriptions = (data, type) =>{
    data.forEach(function(data){
        if ($.inArray(localStorage.college, data.college) === -1) {
            $(`#${type}-courses`).append(`<div class="class-wrap"><div class="input"><input type="checkbox" value="${data.course}" data-className="${data.courseName}" data-course="${data.course}" data-credits="${data.credits}"></div><div class="class-info"><p class="course">${data.course}</p><h3 class="title">${data.courseName}</h3><p class="credits">Credits: ${data.credits}</p><p class="description">${data.description}</p></div></div>`);
        }
    });
};