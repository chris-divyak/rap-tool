/*jshint esversion: 6 */
//----------------------------
// ----- DECISION TREE -------
//----------------------------
const baseUrl = '/assets/rap/';
const versionNumber = (Math.random()+' ').substring(2,10)+(Math.random()+' ').substring(2,10);
const decisionTree = () => {
     console.log('decision tree fired');
     console.log(localStorage);
    //decision tree questions
    $('#decision-tree').append('<div id="counter">2</div><div class="content"><h2>Determine your English class.</h2><div id="decision1" class="decision"><p class="question" tabindex="0">Do you speak English as a first or native language?</p><p class="answeryes" data-val="ENGL015"  data-credits="3" data-semester="Fall" role="alert" aria-live="assertive">Register for ENGL015 Fall Semester</p></div><div id="decision2" class="decision"><p class="question" tabindex="0">Did you attend any English-medium Schools (elem/junior/high) while you were growing up? (Where instruction was delivered in English)</p><p class="answerno" data-val="ESL015"  data-credits="3" data-semester="Fall" role="alert" aria-live="assertive">Register for ESL015 Fall Semester</p></div><div id="decision3" class="decision"><p class="question" tabindex="0">Did you GRADUATE from an English-medium high school (EMHS)?</p><p class="answerno" data-val="ESL015"  data-credits="3" data-semester="Fall" role="alert" aria-live="assertive">Register for ESL015 Fall Semester.</p></div><div id="decision4" class="decision"><p class="question" tabindex="0">Was your EMHS in an English-speaking country?</p><p class="answerno" data-val="ESL015"  data-credits="3" data-semester="Fall" role="alert" aria-live="assertive">Register for ESL015 Fall Semester.</p></div><div id="decision5" class="decision"><p class="question" tabindex="0">Would you rate yourself as a “strong English writer”?</p><p class="answerno" data-val="ESL015" data-credits="3" data-semester="Spring" role="alert" aria-live="assertive">Register for ESL015 Spring Semester.</p><p class="answeryes" data-val="ENGL015" data-credits="3" data-semester="Fall" role="alert" aria-live="assertive">Consider registering for ENGL015 Fall Semester; you may find ESL015 helpful for improving your research &amp; writing skills.</p></div></div>');
    //append button to all
    $('.decision').each(function(){
        $(this).append('<button type="button" value="yes" class="inline btn-success">Yes</button> <button type="button" value="no" class="inline btn-error">No</button>');
    });

    //on button click
    $('button').click(function(e){
        //prevent default button action
        e.preventDefault();

        //find if answers exist in parent element
        const answerNo = $(this).parent().find('.answerno');
        const answerYes = $(this).parent().find('.answeryes');

        
        //value of button
        const val = $(this).attr('value');

        //if button clicked yes
        if(val === 'yes'){
            //if answer exists in parent
            if(answerYes.length === 1){
                //remove all current elements
                $('#decision-tree').remove();
                //show answer
                //$('.answeryes').show().focus();
                //set localstorage values
                const courseVal = answerYes.data('val');
                const semesterVal = answerYes.data('semester');
                const englishCredits = answerYes.data('credits');
                localStorage.setItem("englishCourse", courseVal);
                localStorage.setItem("englishSemester", semesterVal);
                localStorage.setItem("englishCredits", englishCredits);
                if(localStorage.englishSemester === 'Fall'){
                    $('#schedule').append(`<hr/><pre><strong>English Course:</strong> ${localStorage.englishCourse} (<span id="english-credits">${localStorage.englishCredits}</span> Credits)</pre>`);
                }
                 $('#aleks-score, #aleks').show();
            }else{
                //hide parent element
                $(this).parent().hide();
                //show next question
                $(this).parent().next().show().focus();
                //accessible focus on next question
                $(this).parent().next().find('p.question').focus();
            }
            
        } 
        //if button clicked no
        if(val === 'no'){
             //if answer exists in parent
            if(answerNo.length === 1){
                //remove all current elements
                $('#decision-tree').remove();
                //show answer
                //$('.answerno').show().focus();
                //set localstorage values
                const noCourseVal = answerNo.data('val');
                const noSemesterVal = answerNo.data('semester');
                const noSemesterCredit = answerNo.data('credits');
                localStorage.setItem("englishCourse", noCourseVal);
                localStorage.setItem("englishSemester", noSemesterVal);
                localStorage.setItem("englishCredits", noSemesterCredit);
                console.log(localStorage);
                if(localStorage.englishSemester === 'Fall'){
                    $('#schedule').append(`<hr/><pre><strong>English Course:</strong> ${localStorage.englishCourse}(<span id="english-credits">${localStorage.englishCredits}</span> Credits)</pre>`);
                }
                //show aleks on next
                $('#aleks-score, #aleks').show();

            }else{
                //hide parent element
                $(this).parent().hide();
                //show next question
                $(this).parent().next().show();
                //accessible focus on next question
                $(this).parent().next().find('p.question').focus();
            }
         }
    });
};//end of decisionTree()

