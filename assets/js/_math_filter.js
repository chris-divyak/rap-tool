/*jshint esversion: 6 */
//-----------------------------------------------
// ----- START OF MATH CLASS FILTERING ----------
//-----------------------------------------------
const getMathClass = (major) => {
    console.log('getMathClass fired');
    console.log('Major:'+major);
   

    //check if first year seminar
    getFirstSeminar(major);

    $.ajax({
        url: baseUrl+'assets/json/math.json?v='+versionNumber,
        dataType: 'json',
        success: function(data){
            //set localStorage items
            data.forEach(function(data){
                if ($.inArray(major, data.majors) === -1) {
                    localStorage.setItem("math_class", data.course);
                }
            });
            //if no aleks score, run mathFilter();
            if(localStorage.getItem("aleks") !== null || localStorage.getItem("aleks") !== undefined){
                mathFilter(localStorage.aleks, localStorage.math_class);
            }else{
                $('#aleks-score, #aleks-input').show();
            }
        },
        error: function(xhr){
            error(xhr);
        }
    });
};

//----------------------------------------
// ----- START OF MATH FILTERING ---------
//----------------------------------------
const mathFilter = (aleks, mathCourse) => {
    console.log('mathFilter fired');
    //get math JSON data
    aleks = parseInt(aleks);
    console.log('mathcourse: '+mathCourse);

    const course =  mathCourse;
    let math;

    //depending on course, get correct course
    if(course === 'MATH 110 or MATH 140'){
        if(aleks<=13){
            math = 'MATH 3*';
        }
        if(aleks>13 && aleks<30){
            math = 'MATH 4';
        }
        if(aleks>29 && aleks<46){
            math = 'MATH 21';
        }
        if(aleks>45 && aleks<61){
            math = 'MATH 26';
        }
        if(aleks>60 && aleks<76){
            math = 'MATH 110';
        }
        if(aleks>75){
            math = 'MATH 140';
        }
    }

    if(course === 'MATH 140'){
        if(aleks<=13){
            math = 'MATH 3*';
        }
        if(aleks>13 && aleks<30){
            math = 'MATH 4';
        }
        if(aleks>29 && aleks<46){
            math = 'MATH 21';
        }
        if(aleks>45 && aleks<61){
            math = 'MATH 22 & MATH 26';
        }
        if(aleks>60 && aleks<76){
            math = 'MATH 26 OR MATH40* or MATH 41';
        }
        if(aleks>75){
            math = 'MATH 140';
        }
    }
    if(course === 'MATH 26'){
        if(aleks<=13){
            math = 'MATH 3*';
        }
        if(aleks>13 && aleks<30){
            math = 'MATH 4';
        }
        if(aleks>29 && aleks<46){
            math = 'MATH 21';
        }
        if(aleks>45){
            math = 'MATH 26';
        }
    }
    if(course === 'MATH 22'){
        if(aleks<=13){
            math = 'MATH 3*';
        }
        if(aleks>13 && aleks<30){
            math = 'MATH 4';
        }
        if(aleks>29 && aleks<46){
            math = 'MATH 21';
        }
        if(aleks>45){
            math = 'MATH 22';
        }
    }
    if(course === 'MATH 21'){
        math = 'MATH 21';
    }
    if(course === 'STAT 200'){
        if(aleks<=13){
            math = 'MATH 3*';
        }
        if(aleks>13 && aleks<30){
            math = 'MATH 4';
        }
        if(aleks>29){
            math = 'STAT 200';
        }
    }
    // console.log('Final math: '+math);
    localStorage.setItem("math", math);
    getMathCredits(math);
};

//get the math credits associated with the math course
const getMathCredits = (math) => {
    console.log('getMathCredits fired');
    console.log(math);
     $.ajax({
        url: baseUrl+'assets/json/math.json?v='+versionNumber,
        dataType: 'json',
        success: function(data){
            data.forEach(function(data){
                if(math === data.course){
                    localStorage.setItem("math_credits", data.credits);
                    localStorage.setItem("math_class_title", data.courseName);
                    if(localStorage.getItem("aleks") !== null){
                         foreignLanguage(); 
                    }else{
                          decisionTree();
                    }
                }
            });
        },
        error: function(xhr){
            error(xhr);
        }
     });
};

const getFirstSeminar = (major) => {
    console.log('fired first year seminar');
    $.ajax({
        url: baseUrl+'assets/json/colleges_and_majors.json?v='+versionNumber,
        dataType: 'json',
        success: function(data){
            data.forEach(function(data){
                if(data.college === localStorage.college){
                    data.majors.forEach(function(majors){ 
                        if(majors.major === localStorage.major){
                            if(majors.firstSeminarClass !==''){
                                localStorage.setItem('firstSeminarClass', majors.firstSeminarClass);
                                localStorage.setItem('firstSeminarCredits', majors.firstSeminarCredits);
                                console.log(localStorage);
                            }
                        }
                    });
                }
            });
        },
        error: function(xhr){
            //hide all elements on page
            $('#decision-tree, #majors').hide();
            //append custom error message to #errors 
            $('#errors').append('<p class="error">'+xhr.status+' Error: '+xhr.responseText+' Please contact the web administrator.</p>');
        }
    });
};