/*jshint esversion: 6 */
const updateTotal = () =>{
    const englishCredits = parseInt(localStorage.englishCredits);
    const mathCredits = parseInt(localStorage.math_credits);
    let scienceCredits;
    let humanitiesCredits;
    let artsCredits;
    let socialCredits;
    let foreignLanguageCredits;
    let firstYearSeminarCredits;
    let requiredCourseCredits1;
    let requiredCourseCredits2;
    let requiredCourseCredits3;

    if (localStorage.getItem("required_credits1") === null) {
        requiredCourseCredits1 = 0;
    }else{
        requiredCourseCredits1 = parseInt(localStorage.required_credits1);
    }
    if (localStorage.getItem("required_credits2") === null) {
        requiredCourseCredits2 = 0;
    }else{
        requiredCourseCredits2 = parseInt(localStorage.required_credits2);
    }
    if (localStorage.getItem("required_credits3") === null) {
        requiredCourseCredits3 = 0;
    }else{
        requiredCourseCredits3 = parseInt(localStorage.required_credits3);
    }
    console.log('required credits');
    console.log(requiredCourseCredits1 + requiredCourseCredits2+ requiredCourseCredits3);

    if (localStorage.getItem("science_credits") === null) {
        scienceCredits = 0;
    }else{
        scienceCredits = parseInt(localStorage.science_credits);
    }
    if(localStorage.getItem('firstSeminarClass') === null){
        firstYearSeminarCredits = 0;
    }else{
        firstYearSeminarCredits = parseInt(localStorage.firstSeminarCredits);
    }

    if(localStorage.getItem("foreign_language") === "false"){
        foreignLanguageCredits = 0;
    }else{
        foreignLanguageCredits = 4;
    }

    if (localStorage.getItem("humanities_credits") === null) {
        humanitiesCredits = 0;
    }else{
        humanitiesCredits = parseInt(localStorage.humanities_credits);
    }

    if (localStorage.getItem("arts_credits") === null) {
        artsCredits = 0;
    }else{
        artsCredits = parseInt(localStorage.arts_credits);
    }
    if (localStorage.getItem("social_credits") === null) {
        socialCredits = 0;
    }else{
        socialCredits = parseInt(localStorage.social_credits);
    }
    var total = englishCredits + mathCredits + scienceCredits + humanitiesCredits + artsCredits + socialCredits + foreignLanguageCredits + firstYearSeminarCredits + requiredCourseCredits1 + requiredCourseCredits2 + requiredCourseCredits3;
    var credits = $('#total-credits');

    console.log('total credits: '+total);


    if(total > 18){
       credits.html(total+' <br/>Only a maximum of 19 credits are allowed per semester!');
       credits.parent().removeAttr('class').addClass('credits-danger');
    
    }
    if(total>13 && total <19){
        credits.text(total);
        credits.parent().removeAttr('class').addClass('credits-success');
    }
    if(total<14){
        credits.html(total+ ' <br/>We recommend at least 14-17 credits per semester.');
        credits.parent().removeAttr('class').addClass('credits-warning');
    }
};