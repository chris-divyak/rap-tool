/*jshint esversion: 6 */
//----------------------------------------
// ----- START OF MAJOR DATA FETCH -------
//----------------------------------------
//store just majors
let majorData = [];
//store all major data
let allData = [];

//store all colleges
let colleges = [];

const getMajor = () => {
    console.log('getMajor fired');
    
    $.ajax({
        url: baseUrl+'assets/json/colleges_and_majors.json?v='+versionNumber,
        dataType: 'json',
        success: function(data){
            getAllMajors(data, colleges, majorData);
        },
        error: function(xhr){
            //hide all elements on page
            $('#decision-tree, #majors').hide();
            //append custom error message to #errors 
            $('#errors').append('<p class="error">'+xhr.status+' Error: '+xhr.responseText+' Please contact the web administrator.</p>');
        }
    });
    
};//end of getMajor()

//get all majors
const getAllMajors = (data, colleges, majorData) => {
    console.log(data);
    $('#start').remove();
            
    //show majors and colleges menu
    $('#majors, #select-menu').show();
    //hide major list
    $('#major-list, #aleks').hide();
    $('#aleks-score').hide();
    $('#intro, #colleges-list').show();

    //reset localStorage
    $('#colleges-list').val('Pick a College');
    $('#major-list').val('Select a major');
    $('#aleks').val('Select your ALEKS score');

    //push all colleges to list
    if(colleges.length < 1){
        console.log('fire colleges');
        data.forEach(function(data){
            if ($.inArray(data.college, colleges) == -1) {
                colleges.push(data.college);
            }  
        });
        //sort colleges alphabetically
        colleges.sort(function(a, b){
            if(a < b) return -1;
            if(a> b) return 1;
            return 0;
        });
        //append colleges to list
        colleges.forEach(function(colleges){
            $('#colleges-list').append(`<option data-college="${colleges}">${colleges}</option`); 
        });
    }
    
         
    
            
    //on select menu change, append majors for college
    $('#colleges-list').on('change', function(){
        //remove all pre code from majors
        $('#majors pre').remove();                
        //remove all previous options
        $('#major-list').find('option').remove();
        //append default select for major
        $('#major-list').append('<option val="">Select a major</option>');
        //sort majors into select menu
        let sortMajors = $('#colleges-list option:selected').text();
                
        //get all majors in college
        sortedMajors(sortMajors, data, majorData);
    });    
};
//loop through all majors
const sortedMajors = (college, data, majorData) =>{
    //show major list
    $('#major-list').show();
    
        data.forEach(function(data){
            if(data.college === college){
                data.majors.forEach(function(majors){
                  
                        majorData.push(data.majors);
                        $('#major-list').append(`<option data-college=${data.college}>${majors.major} ${majors.type}</option`);
                    
                });
                console.log(majorData);
            }
        });  
    
};
