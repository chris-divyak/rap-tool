/*jshint esversion: 6 */
const foreignLanguage = () => {
    console.log('fired foreign language');
    $.ajax({
        url: baseUrl+'assets/json/colleges_and_majors.json?v='+versionNumber,
        dataType: 'json',
        success: function(data){
           var dataLength = data.length;
           console.log(data);
           data.forEach(function(data){
                let major = localStorage.major.replace(' (B.A.)', '').replace(' (B.S.)', '');
                data.majors.forEach(function(majors){
                    if(majors.major === major){
                        if( majors.foreignLanguageRequired === true){
                            languageSchedule();   
                        }else{
                            generateSchedule();
                        }
  
                    }
                });
           });
        },
        error: function(xhr){
            errors(xhr);
        }
    });
}; //end of generateSchedule()

const languageSchedule = () =>{
    $('#aleks-score').hide();
    $('#foreign-language').show();
    console.log('fired language schedule');
    $.ajax({
        url: baseUrl+'assets/json/foreign_language.json?v='+versionNumber,
        dataType: 'json',
        success: function(data){
           data.forEach(function(data){
            $('#languages-list').append(`<option value="${data.name}">${data.longName}</option>`); 
           });
        },
        error: function(xhr){
            errors(xhr);
        }
    });
};