/*jshint esversion: 6 */
(function($){

$(document).ready(function(){

    $('#aleks-score, #foreign-language').hide();
    localStorage.clear();
    //call majors function on load
     getMajor();

    //on click generate localStorage
    $(document).on('click', '#generate-schedule', function(){
         $('#decision-tree').remove();
         generateSchedule();
    });

    //on select list change
    $('select#colleges-list').on('change', function() {
        let college = $(this).find("option:selected").data('college');
        localStorage.setItem("college", college );
    });

    $('select#major-list').on('change', function() {
        let major = $(this).val().replace(' (B.A.)', '').replace(' (B.S.)', '');
        localStorage.setItem("major", major);
    
       $('#colleges-list, #major-list, #intro').hide();
       if (localStorage.getItem("aleks") === null && localStorage.getItem('englishCourse') !== null || localStorage.getItem("aleks") === "undefined" && localStorage.getItem('englishCourse') !== null) {
           $('#aleks-score, #aleks-input').show();
        }else if(localStorage.getItem("aleks") !== null && localStorage.getItem('englishCourse') === null || localStorage.getItem("aleks") !== "undefined" && localStorage.getItem('englishCourse') === null && localStorage.getItem('foreign_language_type') === null){
            decisionTree();
        }else if(localStorage.getItem("aleks") !== null && localStorage.getItem('englishCourse') === null || localStorage.getItem("aleks") !== "undefined" && localStorage.getItem('englishCourse') !== null && localStorage.getItem('foreign_language_type') === null){
            getMathClass(localStorage.major);
        }else if(localStorage.getItem("aleks") !== "undefined" && localStorage.getItem('englishCourse') !== null && localStorage.getItem('foreign_language_type') === null){
            foreignLanguage();
        } 
        else if(localStorage.getItem("aleks") !== null && localStorage.getItem('englishCourse') === null || localStorage.getItem("aleks") !== "undefined" && localStorage.getItem('englishCourse') !== null && localStorage.getItem('foreign_language_type') !== null){
            generateSchedule();
        }          
       
    });

    $('select#languages-list').on('change', function() {
        let language = $(this).find("option:selected").val();
        localStorage.setItem("foreign_language", language );
        localStorage.setItem("foreign_language_type", language );
    });
    $('select#language-course-list').on('change', function() {
        let languageSection = $(this).find("option:selected").val();
        localStorage.setItem("foreign_language_section", languageSection );

        localStorage.setItem("foreign_language_credits", "4");
    });
    $('select#language-years-list').on('change', function() {
        let languageFour = $(this).find("option:selected").val();
        localStorage.setItem("foreign_four_year", languageFour );
        console.log(localStorage);
    });
    $('#save-foreign-language').click(function(){
        generateSchedule();
    });

    $('#save-aleks').click(function(e){
        e.preventDefault();
        let aleks = $('input#aleks-input').val();
        console.log(aleks);
        localStorage.setItem("aleks", aleks);
        getMathClass(localStorage.major);

    });
    

    $(document).on('click', '#select-science', function(){
        getCourses('assets/json/sciences.json?v='+versionNumber, 'science');
        $(this).hide();
        $('#science-courses').show();
    });
    $(document).on('click', '#select-humanities', function(){
        getCourses('assets/json/humanities.json?v='+versionNumber, 'humanities');
        $(this).hide();
        $('#humanities-courses').show();
    });
    $(document).on('click', '#select-arts', function(){
        getCourses('assets/json/arts.json?v='+versionNumber, 'arts');
        $(this).hide();
        $('#arts-courses').show();
    });
    $(document).on('click', '#select-social', function(){
        getCourses('assets/json/social.json?v='+versionNumber, 'social');
        $(this).hide();
        $('#social-courses').show();
    });

    let savedData = [];
     $(document).on('click', '#save', function(){
        //$(this).hide();
        $(this).append();
        savedData.push(localStorage);
        console.log(JSON.stringify(savedData));

        $(this).after('<pre>'+JSON.stringify(savedData)+'</pre>');
        
    });
     $(document).on('click', '#reset', function(){
        //remove schedules
        $('#majors #schedule').each(function(){
            $(this).remove();
            console.log('removed');
        });
        $('#foreign-language').hide();

        //localStorage clear;
        localStorage.removeItem('college');
        localStorage.removeItem('major');
        // localStorage.removeItem('math');
        // localStorage.removeItem('math_class');
        // localStorage.removeItem('math_credits');
        // localStorage.removeItem('math_class_title');

        localStorage.removeItem("arts_credits");
        localStorage.removeItem('arts_course');
        localStorage.removeItem('arts_title');

        localStorage.removeItem("humanities_credits");
        localStorage.removeItem('humanities_course');
        localStorage.removeItem('humanities_title');

        localStorage.removeItem("science_credits");
        localStorage.removeItem('science_course');
        localStorage.removeItem('science_title');

        localStorage.removeItem("social_credits");
        localStorage.removeItem('social_course');
        localStorage.removeItem('social_title');

        localStorage.removeItem('firstSeminarClass');
        localStorage.removeItem('firstSeminarCredits');

        localStorage.removeItem('science_credits');
        localStorage.removeItem('science_course');

        localStorage.removeItem('required_course1');
        localStorage.removeItem('required_credits1');

        localStorage.removeItem('required_course2');
        localStorage.removeItem('required_credits2');

        localStorage.removeItem('required_course3');
        localStorage.removeItem('required_credits3');

        localStorage.removeItem('required_course4');
        localStorage.removeItem('required_credits4');

        scheduleData =[];
        console.log(localStorage);
        getMajor();

    });
});

const error = (xhr) =>{
    //hide all elements on page
    $('#decision-tree, #majors').hide();
    //append custom error message to #errors
    $('#errors').append(`getMathCredits error:<p class="error">${xhr.status} Error: ${xhr.responseText} Please contact the web administrator.</p>`);
};

})(jQuery);

