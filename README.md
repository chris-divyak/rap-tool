WPSU Boilerplate with Gulp
==========================

## Check out Gulp configuration tutorial at: [https://travismaynard.com/writing/getting-started-with-gulp]

==========================

### File Setup

- assets
	- css
	- images
	- js
		- scripts.js (main scripts file)
	- min
		- scripts.min.js (compiled scripts file)
		- main.css (compiled CSS file)
	- scss
		- main.scss (main SASS file)
- gulpfile.js (main Gulp config file)
- index.html (includes CSS, Javascript, and jQuery by default)
- package.json (to run Gulp)

==========================


### Get Started

1. Go to the command line and access your file directory. Example

``` cd /Applications/XAMPP/xamppfiles/htdocs/global-programs/PSPM-dissatraining_psu_edu ```
=======
### Getting Started

##### 1. Go to the command line and access your file directory. Example

``` 
cd /Applications/XAMPP/xamppfiles/htdocs/global-programs/PSPM-dissatraining_psu_edu 
```


##### 2. Install Node.js by going to [https://nodejs.org/en/]

##### 3. Install Gulp globally

```
npm install -g gulp
```

##### 4. Install Gulp and all dependencies in project locally


```npm install --save-dev```


##### 5. Run Gulp

```
gulp
```

Gulp already has a watch script that updates the CSS and Javascript min files on save. No need to run an additional compiler.