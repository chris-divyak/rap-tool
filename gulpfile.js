// Include gulp
const gulp = require('gulp');

// Include Our Plugins
const jshint = require('gulp-jshint');
const sass = require('gulp-sass');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
var ts = require('gulp-typescript');
const minifyJS = require('gulp-minify');
const pump = require('pump');
const rename = require('gulp-rename');
const babel = require('gulp-babel');
const rev = require('gulp-rev-append');

// Lint Task
gulp.task('lint', function() {
    return gulp.src('assets/js/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

// Compile Our Sass
gulp.task('sass', function() {
    return gulp.src('assets/scss/*.scss')
        .pipe(sass())

        .pipe(gulp.dest('assets/min/'))
});

// Concatenate & Minify JS
gulp.task('scripts', function() {
  return gulp.src('assets/js/*.js')

    .pipe(babel({presets: ['es2015']}))
    .pipe(concat('scripts.min.js'))
    .pipe(ts({
        target: "es5",
        allowJs: true,
        module: "commonjs",
        moduleResolution: "node"
    }))
    //.pipe(uglify())
    .pipe(gulp.dest('assets/min/'))
});


//add revision number to template - disable cached files
gulp.task('rev', function() {
  gulp.src('./index.html')
    .pipe(rev())
    .pipe(gulp.dest('.'));
});

// Watch Files For Changescd assets/rap
gulp.task('watch', function() {
    gulp.watch('assets/js/*.js', ['lint', 'scripts', 'rev']);
    gulp.watch('assets/scss/*.scss', ['sass', 'rev']);
});

// Default Task
gulp.task('default', ['lint', 'sass', 'scripts', 'watch', 'rev']);